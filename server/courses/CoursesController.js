'use strict';
var Rotor = require('../rotor/rotor');

var CoursesController = Rotor.Controller.extend({
	collection: require('./Models/CoursesList')
});

module.exports = new CoursesController();