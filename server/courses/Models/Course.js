'use strict';
var Backbone = require('../../rotor/rotor');

var Course = Backbone.Model.extend({
	name: 'courses',
	defaults: {
		city: '',
		teachers: '',
		groups: ''
	} ,

});

module.exports = Course;